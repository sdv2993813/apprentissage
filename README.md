# Apprentissage

<a href="https://gitpod.io/#https://gitlab.com/sdv2993813/apprentissage/-/tree/main/">
    <img src="https://img.shields.io/badge/Gitpod-000000?style=for-the-badge&logo=gitpod&logoColor=#FFAE33" alt="Gitpod ready-to-code" />
  </a>

Ce projet vise à fournir une plateforme d'apprentissage pour aider les débutants à acquérir des compétences en codage et en scripting. L'objectif principal est de rendre l'apprentissage de la programmation accessible à tous, quel que soit leur niveau de compétence actuel. Le projet vise à offrir un environnement d'apprentissage interactif et pratique pour aider les apprenants à développer leurs compétences et leur confiance en matière de codage.

## Fonctionnalités

- **Exercices pratiques**: Des exercices pratiques sont inclus pour permettre aux apprenants de mettre en pratique ce qu'ils ont appris dans les modules de cours.
- **Support de plusieurs langages**: Le projet prend en charge une variété de langages de programmation et de scripting, tels que Python, bash etc... Hésite pas à me dire quel langage tu veux apprendre.

## Environnement de développement

Pour utiliser ce dépôt, tu n'auras pas besoin de télécharger d'outils. Tout se passera par [Gitpod](https://www.gitpod.io/).  
Il te suffit de cliquer sur le bouton Gitpod en haut de ce fichier README.md.

## Conseils

Les meilleurs conseils que je peux te donner :  
- Apprends le Markdown
- **RTFM**
- Sois curieux

## Comment participer ?

- Créer ta branche avec ton prénom à partir de la `main`
- Créer une milestone avec un nom explicite et court
- Créer une issue par exercice
  - **⚠️ le nom de l'issue doit être explicite et court**
  - Rattacher l'issue à la milestone
  - Mettre les labels nécessaires

A partir de l'issue :  
- Créer une merge request qui à comme source ta branche et comme destination ta branche
- Rattache la merge request à ta milestone

Pour valider un exercice il faut que le job de la pipeline soit en succès. A partir de là, tu peux mettre le label `To-Review` pour que je puisse te corriger et fusionner ta branche de travail, sur ta branche perso.  

***Si t'as besoin d'aide, laisse un message dans la merge request et ping moi avec > @FlorianD78340***