import unittest

def generate_even_numbers():
    """
    Génère une liste de nombre pairs de 1 à 100.
    """
    return None

class TestGenerateEvenNumbers(unittest.TestCase):
    def test_generate_even_numbers(self):
        expected_output = [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 62, 64, 66, 68, 70, 72, 74, 76, 78, 80, 82, 84, 86, 88, 90, 92, 94, 96, 98, 100]
        self.assertEqual(generate_even_numbers(), expected_output)

if __name__ == '__main__':
    unittest.main()