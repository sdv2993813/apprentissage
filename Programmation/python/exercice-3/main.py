import unittest

def fibonacci(n):
    """
    Génère la suite de Fibonacci
    """
    return None

class TestFibonacci(unittest.TestCase):
    def test_fibonacci_sequence(self):
        # Test for the first 10 terms of Fibonacci sequence
        expected_output = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34]
        self.assertEqual(fibonacci(10), expected_output)

if __name__ == '__main__':
    unittest.main()