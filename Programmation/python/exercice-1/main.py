import unittest

def is_common_digits(liste1, liste2):
  """
  is_common_digits() renvoie True s'il y a un chiffre en communs dans les listes
  """
  return_value = None

  return return_value

def common_digit(liste1, liste2):
  """
  common_digits() renvoie le chiffre en communs dans les listes
  """

  return common_digit

class TestStringMethods(unittest.TestCase):

  def test_is_common_digits_with_common_digits(self):
      liste1 = [12, 65, 987, 315, 62]
      liste2 = [95, 31, 8, 315, 7]
      self.assertEqual(is_common_digits(liste1, liste2), 315)

  def test_is_common_digits_without_common_digits(self):
      liste1 = [12, 65, 987, 315, 62]
      liste2 = [95, 81, 74, 520, 7]
      self.assertEqual(is_common_digits(liste1, liste2), False)

  def test_common_digits_with_common_digits(self):
      liste1 = [12, 65, 987, 315, 62]
      liste2 = [95, 31, 8, 315, 7]
      self.assertEqual(common_digit(liste1, liste2), '3')

  def test_common_digits_without_common_digits(self):
      liste1 = [12, 65, 987, 315, 62]
      liste2 = [95, 81, 74, 520, 7]
      self.assertEqual(common_digit(liste1, liste2), None)

if __name__ == '__main__':
    unittest.main()
