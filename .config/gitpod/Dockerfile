# This Dockerfile establishes the foundation for a containerized environment, particularly for the Gitpod setup. 
# It starts with a stable Ubuntu image to ensure a reliable and consistent operating system base.

FROM ubuntu:22.04 AS builder

# Using the root user for installations ensures unrestricted access to system directories and resources,
# which is necessary for installing system-wide packages and performing administrative tasks.
# hadolint ignore=DL3002
USER root

# Set the working directory
WORKDIR /workspace/gitpod

# The init.sh script is copied into the Docker image to set up the environment.
# This includes installing various tools and configurations required for Gitpod's operation.
COPY . .

# Marking the script as executable and running it ensures that all the necessary environment setups
# and configurations are in place when the Docker image is built.
RUN pwd && ls -lisa && chmod +x .config/gitpod/init.sh && .config/gitpod/init.sh

# Creating a dedicated 'gitpod' user provides a non-root user environment for operations, enhancing security.
# The specific UID, group, home directory, and default shell are set for consistency and compliance with expected user settings.
# hadolint ignore=DL3059
RUN useradd -l -u 33333 -G sudo -md /home/gitpod -s /bin/bash -p gitpod gitpod \
    && echo 'gitpod ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

USER gitpod

# Set environment variable PATH to include /home/gitpod/.local/bin
ENV PATH="/home/gitpod/.local/bin:${PATH}"

# hadolint ignore=DL3059
RUN pwd && ls -lisa && .config/gitpod/init.sh

# The final stage of the Dockerfile transitions to a scratch image. 
# This approach is used for creating a minimalistic final image by copying only the necessary files, 
# ensuring a smaller footprint and reduced security risk.
FROM scratch

# Copying essential files from the builder stage to the final image ensures that only the required components
# are included, thus maintaining a lean and secure container environment.
COPY --from=builder / /

USER gitpod
