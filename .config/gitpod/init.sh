#!/bin/bash

# Use non-interactive frontend for debconf
export DEBIAN_FRONTEND=noninteractive

check_and_install_sudo() {
    if ! command -v sudo &>/dev/null; then
        apt-get update -y
        apt-get install -y sudo
    fi
}

upgrade_system() {
    sudo apt-get update
    sudo apt-get upgrade -y
}

install_packages() {
    PACKAGES=(
        apt-utils
        bash-completion
        ca-certificates
        curl
        git
        git-lfs
        gnupg
        iproute2
        lsb-release
        sudo
        wget
    )

    sudo apt-get update -y
    sudo apt-get install -y --no-install-recommends "${PACKAGES[@]}"
}

install_task() {
    curl -sL https://taskfile.dev/install.sh | sudo sh -s -- -d -b /usr/local/bin

    task --version
}

install_docker() {
    sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor | sudo tee /usr/share/keyrings/docker-archive-keyring.gpg >/dev/null
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list >/dev/null
    sudo apt-get update
    sudo apt-get install -y --no-install-recommends docker-ce docker-ce-cli containerd.io
}

cleanup() {
    sudo apt-get clean
    sudo rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
}

# setup_rest_of_system function handles the installation of a set of essential packages,
# which are crucial for a fully functional and versatile development environment.
# This includes tools for code editing, file navigation, version control, and more.
setup_rest_of_system() {
    # Check if the current user is root (UID 0)
    if [[ $(id -u) -eq 0 ]]; then
        echo "setup_rest_of_system() is skipped for root user during Docker build."
        return 0 # Return success to avoid failing the Docker build
    fi

    # Append the tox path to PATH, if necessary
    TOX_PATH="$HOME/.local/bin"
    if [[ ":$PATH:" != *":$TOX_PATH:"* ]]; then
        echo "export PATH=\$PATH:$TOX_PATH" >>~/.bashrc
        echo "export PATH=\$PATH:$TOX_PATH" >>~/.bash_profile
        source ~/.bashrc
    fi

    # Setting up Task completions for Bash enhances the developer experience by providing
    # auto-completion capabilities for Task commands, thus increasing productivity and ease of use.

    mkdir -p ~/.bash_completion.d/
    curl -sL https://raw.githubusercontent.com/go-task/task/main/completion/bash/task.bash -o ~/.bash_completion.d/task.bash
    chmod +x ~/.bash_completion.d/task.bash

    # This check ensures that Task completions are sourced only once in the bash profile,
    # avoiding unnecessary duplication and keeping the profile configuration clean and efficient.
    if [ ! -f ~/.bash_profile ]; then
        touch ~/.bash_profile
    fi

    COMPLETION_SCRIPT="source ~/.bash_completion.d/task.bash"
    if ! grep -qF -- "$COMPLETION_SCRIPT" ~/.bash_profile; then
        echo "$COMPLETION_SCRIPT" >>~/.bash_profile
    fi
}

check_and_install_sudo
upgrade_system
install_packages
install_docker
install_task
task python:init
cleanup
setup_rest_of_system
